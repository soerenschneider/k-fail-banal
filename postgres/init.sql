CREATE TABLE IF NOT EXISTS kfailb (                                                    
    id SERIAL PRIMARY KEY,
    line SMALLINT,
    what TEXT,
    time_stamp TIMESTAMP,
    stations JSONB
);

CREATE TABLE IF NOT EXISTS kfailb_history (
    id BIGSERIAL PRIMARY KEY,
    line SMALLINT,
    what TEXT,
    time_stamp TIMESTAMP,
    stations JSONB
);

ALTER DATABASE kfailb SET timezone TO 'Europe/Berlin';

DROP USER IF EXISTS grafanareader;
CREATE USER grafanareader WITH PASSWORD 'pass';
GRANT USAGE ON SCHEMA public TO grafanareader;
GRANT SELECT ON kfailb TO grafanareader;
