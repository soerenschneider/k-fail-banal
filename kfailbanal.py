import logging
import redis
import time
import json
import configargparse
import psycopg2
import hashlib
import sys
import datetime


_key_all = "kfailb_all"
_key_line = "kfailb_line_{}"


class ProcessedHashesRedisBackend:
    def __init__(self, redis, key='kfailbanal_processed_hashes'):
        self._redis = redis
        self._key = key

    def read_processed_hashes(self):
        """ returns a list of hashes"""
        hashes = self._redis.get(self._key)
        if not hashes:
            logging.debug(f'No previously processed hashes found')
            return {}

        logging.debug(f'Read processed hashes: {hashes}')
        return hashes.decode('utf-8')

    def is_hash_processed(self, hash):
        """ checks whether a hash has been processed lately. """

        # if it has a score it has been processed, otherwise it hasn't
        score = self._redis.zscore(self._key, hash)
        return score is not None

    def write_processed_hashes(self, hashes):
        """ stores the processed hashes in the backend. """

        # the hashes are stored as a sorted set, the score is determined by the current unix timestamp
        rank = int(time.time())

        # we want to delete everything that's older than the timestamp one hour ago
        older_than_1h = rank - 60 * 60

        # add all the hashes in a pipe for more efficiency
        pipe = self._redis.pipeline()
        for h in hashes:
            pipe.zadd(self._key, {h: rank})
        pipe.zremrangebyscore(self._key, 0, older_than_1h)
        pipe.execute()

    def get_data(self):
        cache_data = _redis.get(_key_all)
        if cache_data:
            return json.loads(cache_data.decode('utf-8'))
        return dict()


class ProcessedHashes:
    @staticmethod
    def filter_stale_data(hash_backend):
        """ Returns a subset of the list that has not been seen before. """
        processed_hashes = []

        ret = []

        cache_data = _hashes_backend.get_data()
        for incident in cache_data['incidents']:
            hashed = get_hash_for_incident(incident)
            processed_hashes.append(hashed)

            if not hash_backend.is_hash_processed(hashed):
                logging.debug(f'{hashed} [{incident}] not found in prev_processed_hashes')

                # attach timestamp from original object
                incident['timestamp'] = cache_data['time_stamp']
                ret.append(incident)

        hash_backend.write_processed_hashes(processed_hashes)
        write_history_to_db(cache_data['incidents'])

        return ret


_args = None
_redis = None

_hashes_backend = None
_db_name = "kfailb"
_table_name = "kfailb"
_table_name_history = f"{_db_name}_history"
_reconnect_retries = 5
_events_key = 'kfailb_events'


def backup(data):
    logging.error(f'Couldn\'t write {data} to db.')


def write_history_to_db(data):
    write_to_db(data, _table_name_history)


def write_changes_to_db(data):
    write_to_db(data, _table_name)


def write_to_db(data, table_name):
    try:
        with psycopg2.connect(host=_args.pg_host, user=_args.pg_user, password=_args.pg_pw, dbname=_db_name) as db:
            with db.cursor() as cursor:
                for incident in data:
                    if 'stations' in incident:
                        sql = f"INSERT INTO {table_name} (line, what, time_stamp, stations) VALUES(%s, %s, %s, %s) ON CONFLICT DO NOTHING"
                        cursor.execute(sql, (incident["line"], incident["what"], incident.get('timestamp', datetime.datetime.now()), json.dumps(incident['stations'])))
                    else:
                        sql = f"INSERT INTO {table_name} (line, what, time_stamp) VALUES(%s, %s, %s) ON CONFLICT DO NOTHING"
                        cursor.execute(sql, (incident["line"], incident["what"], incident.get('timestamp', datetime.datetime.now())))
    except Exception as e:
        logging.error(f'Couldn\'t write to database: {e}')
        backup(data)


def get_hash_for_incident(incident):
    return hashlib.sha256(str(incident).encode('utf-8')).hexdigest()


def idle():
    global _redis, _hashes_backend
    
    _redis = redis.Redis(host=_args.redis_host, port=6379, db=0)
    _hashes_backend = ProcessedHashesRedisBackend(_redis)
    pubsub = _redis.pubsub()
    pubsub.psubscribe(_args.topic)
    for msg in pubsub.listen():
        if msg['type'] == 'pmessage':
            try:
                new_data = ProcessedHashes.filter_stale_data(_hashes_backend)
                if new_data:
                    write_changes_to_db(new_data)

            except KeyboardInterrupt as e:
                logging.error(str(e))


def init():
    loglevel = logging.DEBUG
    if _args.debug:
        loglevel = logging.DEBUG
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=loglevel)

    retries = _reconnect_retries
    connected = False
    logging.info(f'Trying to connect to database on {_args.pg_host}')
    while retries >= 0 and not connected:
        try:
            db = psycopg2.connect(host=_args.pg_host, user=_args.pg_user, password=_args.pg_pw, dbname=_db_name)
            connected = True
            db.close()
            logging.info(f'Successfully connected to database')
        except psycopg2.OperationalError as e:
            retries -= 1
            sleep = _reconnect_retries - retries
            logging.info(f"Can't connect to db, trying again in {sleep} seconds")
            time.sleep(sleep)

    if not connected:
        logging.fatal("Couldn't connect to database, giving up. ")
        sys.exit(1)


def parse_args():
    """ Argparse stuff happens here. """
    parser = configargparse.ArgumentParser(prog='k-fail-bot')

    parser.add_argument('--topic', action="store", env_var="KFAILBANAL_TOPIC", default=_events_key)
    parser.add_argument('-r', '--redis-host', dest="redis_host", action="store", env_var="KFAILBANAL_REDIS", required=True)
    parser.add_argument('--pg-host', dest="pg_host", action="store", env_var="KFAILBANAL_PG_HOST", required=True)
    parser.add_argument('--pg-user', dest="pg_user", action="store", env_var="KFAILBANAL_PG_USER", required=True)
    parser.add_argument('--pg-pw', dest="pg_pw", action="store", env_var="KFAILBANAL_PG_PW", required=True)
    parser.add_argument('--debug', dest="debug", action="store_true", env_var="KFAILBANAL_DEBUG", default=False)

    global _args
    _args = parser.parse_args()


if __name__ == '__main__':
    parse_args()
    init()
    idle()
