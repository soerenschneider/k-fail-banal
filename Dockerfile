FROM python:3.7-slim

COPY requirements.txt /opt/kfailbanal/requirements.txt
WORKDIR /opt/kfailbanal
RUN pip install --no-cache-dir -r requirements.txt

COPY kfailbanal.py /opt/kfailbanal

RUN useradd toor
USER toor

CMD ["python3", "kfailbanal.py"]
